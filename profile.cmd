@echo off
if not exist "%TEMP%\.ssh\SSH_AGENT_PID.env" pwsh -C "Write-Host $(Get-Process | Where-Object { $_.Name -eq 'ssh-agent' } | Select-Object -ExpandProperty Id -First 1)" > %TEMP%\.ssh\SSH_AGENT_PID.env
set /P SSH_AUTH_SOCK=<"%TEMP%\.ssh\SSH_AUTH_SOCK.env"
set /P SSH_AGENT_PID=<"%TEMP%\.ssh\SSH_AGENT_PID.env"
