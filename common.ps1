# Creates a directory
function CreateDirectory([string]$path)
{
	New-Item -ItemType Directory -Path $path -ErrorAction SilentlyContinue
}

# Gets a registry key value.
function GetKey([string]$path, [string]$name)
{
	return Get-ItemPropertyValue -LiteralPath $path -Name $name
}

# Creates a new registry entry if one doesn't exist already, otherwise modifies the already existing one
function SetKey([string]$path, [string]$name, $value, [string]$type = "String")
{
	$item = Get-Item -LiteralPath $path -ErrorAction SilentlyContinue

	if ($item -eq $null)
	{
		$item = New-Item -Path $path -Force
	}

	$value_name = $name

	if ($name -eq "(Default)")
	{
		$value_name = ""
	}

	$cur_value = $item.GetValue($value_name)

	if ($cur_value -eq $null)
	{
		Write-Host "Creating registry entry: $path\$name; $type; $value"
		New-ItemProperty -LiteralPath $path -Name $name -PropertyType $type -Value $value | Out-Null
	}
	elseif (($cur_value -ne $value) -or ($item.GetValueKind($value_name) -ne $type))
	{
		Write-Host "Modifying registry entry: $path\$name; $type; $value"
		Set-ItemProperty -LiteralPath $path -Name $name -Value $value -Type $type | Out-Null
	}
}

# Copies an item from $path to $destination
function CopyItem([string]$path, [string]$destination)
{
	Copy-Item -LiteralPath $path -Destination $destination
}

# Moves the item at $path to $destination
function MoveItem([string]$path, [string]$destination)
{
	Write-Host "Moving $path to $destination"
	Move-Item -LiteralPath $path -Destination $destination
}

# Deletes registry subkey $name at $path
function DeleteItem([string]$path, [string]$name)
{
	$item = Get-ItemProperty -LiteralPath $path -Name $name -ErrorAction SilentlyContinue

	if ($item)
	{
		Write-Host "Deleting registry value $path\$name"
		Remove-ItemProperty -LiteralPath $path -Name $name
	}
}

# Deletes the key at $path. Also deletes all the children by default.
function DeleteKey([string]$path, [bool]$recurse = $true)
{
	if (Test-Path -LiteralPath $path)
	{
		Write-Host "Deleting $path"

		if ($recurse)
		{
			Remove-Item -LiteralPath $path -Recurse
		}
		else
		{
			Remove-Item -LiteralPath $path
		}
	}
}

# Font removal
$shell = New-Object -COM "Shell.Application"
Set-Variable CSIDL_FONTS 0x14 -Option Constant
Set-Variable fonts_folder ($shell.NameSpace($CSIDL_FONTS).Self.Path) -Option Constant
Set-Variable fonts_registry "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts" -Option Constant

Add-Type -TypeDefinition @"
using System;
using System.Runtime.InteropServices;

public class FontUtils
{
[DllImport("gdi32.dll")]
private static extern int AddFontResource(string lpFilename);

[DllImport("gdi32.dll")]
private static extern int RemoveFontResource(string lpFileName);

[return: MarshalAs(UnmanagedType.Bool)]
[DllImport("user32.dll", SetLastError=true)]
private static extern bool PostMessage(IntPtr hWnd, WM Msg, IntPtr wParam, IntPtr lParam);

private static IntPtr HWND_BROADCAST = new IntPtr(0xffff);

private enum WM : uint
{
	FONTCHANGE = 0x1D,
}

public static bool AddFontNative(string file)
{
	bool success = AddFontResource(file) != 0;
	PostMessage(HWND_BROADCAST, WM.FONTCHANGE, IntPtr.Zero, IntPtr.Zero);
	return success;
}

public static bool RemoveFontNative(string file)
{
	bool success = RemoveFontResource(file) != 0;
	PostMessage(HWND_BROADCAST, WM.FONTCHANGE, IntPtr.Zero, IntPtr.Zero);
	return success;
}
}
"@

function AddFont([string]$path)
{
	if (!(Test-Path $path))
	{
		Write-Error "Font $path doesn't exist"
		return
	}

	$folder_path = Split-Path -Resolve -Parent $path
	$file_name = Split-Path -Leaf $path
	$folder = $shell.NameSpace($folder_path)
	$name = ""

	foreach ($item in $folder.Items())
	{
		if ((Split-Path -Leaf $item.Path()) -eq $file_name)
		{
			$name = $folder.GetDetailsOf($item, 21) # Title
		}
	}

	$existing = Get-ItemProperty -LiteralPath $fonts_registry -Name $name -ErrorAction SilentlyContinue
	$destination = "$fonts_folder\$file_name"

	if ($existing -and ($existing.$name -eq $file_name) -and (Get-FileHash $path).Hash -eq (Get-FileHash $destination).Hash)
	{
		return
	}

	if ($existing)
	{
		Write-Host "Overwriting font $name ($file_name)"
	}
	else
	{
		Write-Host "Adding font $name ($file_name)"
		SetKey $fonts_registry $name $file_name
	}

	Remove-Item -LiteralPath $destination
	Move-Item -LiteralPath $path -Destination $destination
	[FontUtils]::AddFontNative($destination) | Out-Null
	SetKey $fonts_registry $name $file_name
}

function RemoveFont([string]$file)
{
	$name = ""

	foreach ($property in (Get-ItemProperty $fonts_registry).PsObject.Properties)
	{
		if ($property.Value -eq $file)
		{
			$name = $property.Name
			break
		}
	}

	$font_path = Join-Path $fonts_folder $file

	if ($name -eq "")
	{
		# Removing right after deregistering is unlikely to work due to the font being in use.
		if (Test-Path $font_path)
		{
			Write-Host "Removing $font_path"
			Remove-Item $font_path | Out-Null
		}

		return
	}

	Write-Host "Removing font $name ($file)"
	[FontUtils]::RemoveFontNative($font_path) | Out-Null
	DeleteItem $fonts_registry $name
}

# Download if not already done.
New-Item -Type Directory "files" -Force | Out-Null

function Download([string]$uri, [string]$out = (Split-Path -Leaf $uri))
{
	if (!(Test-Path "files/$out"))
	{
		Write-Host "Downloading $out..."
		Invoke-WebRequest -Uri $uri -Method "GET" -OutFile "files/$out"
	}
}

function Extract([string]$file, [string]$out)
{
	if (!(Test-Path "files/$out"))
	{
		Write-Host "Extracting $file..."
		Expand-Archive -Path "files/$file" -DestinationPath "files/$out"
	}
}
