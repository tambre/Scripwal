$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

$scripts = "$env:ProgramFiles\Python\Scripts"
$ttx = "$scripts\ttx.exe"
$pyftmerge = "$scripts\pyftmerge.exe"
$seguiemj = "$env:SystemRoot\Fonts\seguiemj.ttf"
$seguisym = "$env:SystemRoot\Fonts\seguisym.ttf"
$noto = "Noto-COLRv1-emojicompat.ttf"

# Merge Segoe UI Emoji and Symbol name tables with Noto Color Emoji so Windows won't ignore it.
Invoke-WebRequest -Uri "https://github.com/googlefonts/noto-emoji/raw/main/fonts/NotoColorEmoji_WindowsCompatible.ttf" -Method "GET" -OutFile $noto
& $ttx -t "name" -o "name.ttx" $seguiemj
& $ttx -o "seguiemj.ttf" -m $noto "name.ttx"
& $ttx -t "name" -o "name.ttx" $seguisym
& $ttx -o "seguisym.ttf" -m $noto "name.ttx"
Remove-Item name.ttx, $noto
