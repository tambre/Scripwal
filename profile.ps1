param([string]$working_directory = $null)

# Set location if passed as an argument.
if ($working_directory)
{
	Set-Location -LiteralPath $working_directory
}

# Alias git
Set-Alias -Name git -Value "$env:ProgramFiles/Git/bin/git.exe"

# Import posh-git
Import-Module posh-git

# Remove curl alias, it's annoying.
While (Test-Path Alias:curl)
{
	Remove-Item Alias:curl
}

function msvc
{
	$instance = (& "${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe" -prerelease -latest -format json | ConvertFrom-Json)
	Import-Module "$($instance.installationPath)\Common7\Tools\Microsoft.VisualStudio.DevShell.dll"
	Enter-VsDevShell $instance.instanceId -DevCmdArguments '-arch=x64 -no_logo' -SkipAutomaticLocation
}

# Use the non-official OpenSSH for Windows distribution, which ships a newer version that's more likely to work.
Import-Module posh-sshell

# The agent reports a Cygwin PID, which the import reads. Override it to the real Windows PID.
$env:SSH_AGENT_PID = Get-Process | Where-Object { $_.Name -eq 'ssh-agent' } | Select-Object -ExpandProperty Id -First 1

Set-Alias ssh "$env:ProgramFiles\OpenSSH\bin\ssh.exe"
Set-Alias ssh-agent "$env:ProgramFiles\OpenSSH\bin\ssh-agent.exe"
Set-Alias ssh-add "$env:ProgramFiles\OpenSSH\bin\ssh-add.exe"
Start-SshAgent -Quiet
